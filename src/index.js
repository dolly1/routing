import React from 'react'
import ReactDOM from 'react-dom'
import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom'

import './index.css'
import App from './App'
import Users from './Users'
import Contact from './Contact'
import Header from './Header'
import Notfound from './Notfound'


const routing = (
  <Router>
    <div>
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/users" component={Users} />
        <Route path="/contact" component={Contact} />
        <Route component={Notfound} />
      </Switch>
    </div>
  </Router>
)

ReactDOM.render(<Header/>, document.getElementById('headdiv'))
ReactDOM.render(routing, document.getElementById('root'))